//全屏
 public void setFullscreen() { 
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN); 
    } 
    
//无标题
    public void setNoTitle() { 
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
    } 